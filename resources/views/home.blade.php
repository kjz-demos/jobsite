@extends('layouts.app')
@section('title')
    {{ __ucf('home') }}
@endsection
@section('results-title')
    {{ __ucf('latest jobs') }}
@endsection

@section('content')
    <section class="section">
        <div class="row">
            <div class="col-12 mb-4">
                <div class="hero text-white hero-bg-image" style="background-image: url('./assets/images/hero-bg.jpg');">
                    <div class="hero-inner">
                        <h2>Browse by category</h2>
                        <div class="mt-4">
                            @foreach($categories as $category)
                                <a href="{{ route('categories.show', $category) }}" class="btn btn-outline-white btn btn-icon icon-left ml-2 mb-2">
                                    <i class="far fa-folder"></i>
                                    {{ $category->name }} ({{ $category->jobs()->count() }})
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 mb-4">
                @include('jobs.table', $jobs)
            </div>
            <div class="col-md-3 mb-4">
                @include('tags.cloud', $tags)
            </div>
        </div>

    </section>
@endsection

