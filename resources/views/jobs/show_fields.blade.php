<div class="card profile-widget">
    <div class="profile-widget-header">
        <img alt="{{ $job->company->name }}"
             src="{{ asset($job->company->logo ?? 'assets/images/missing-logo.svg') }}" class="job-details-logo">
        <div class="profile-widget-items">
            <div class="profile-widget-item">
                <div class="profile-widget-item-label">Min salary</div>
                <div class="profile-widget-item-value">{{ money($job->salary_min) }}</div>
            </div>
            <div class="profile-widget-item">
                <div class="profile-widget-item-label">Max salary</div>
                <div class="profile-widget-item-value">{{ money($job->salary_min) }}</div>
            </div>
            <div class="profile-widget-item">
                <div class="profile-widget-item-label">Start date</div>
                <div class="profile-widget-item-value">{{ human_date($job->start_date) }}</div>
            </div>
        </div>
    </div>
    <div class="profile-widget-description">
        <div class="profile-widget-name">{{ $job->title }}
            <div class="text-muted d-inline font-weight-normal">
                <div class="slash"></div> {{ $job->company->name }}</div>
        </div>
        {!! $job->description !!}
    </div>
    <div class="card-footer">
        <div class="mb-2">
            <span class="font-weight-bold">Category: </span><a href="{{ route('categories.show', $job->category) }}"
                                                               class="text-info">{{ $job->category->name }}</a>
        </div>
        @if ($job->tags->count())
            <div class="mb-2">
                <span class="font-weight-bold">Tags: </span>
                @foreach ($job->tags as $tag)
                    <a href="{{ route('tags.show', $tag) }}" class="btn-sm btn-info"><i
                            class="fa fa-tag mr-1"></i>{{ $tag->name }}</a>
                @endforeach
            </div>
        @endif
        @if(auth_admin() && $job->status == 'pending')
            <a href="{{ route('jobs.approve', $job) }}" class="btn btn-warning">Approve</a>
        @endif
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h4>About the company</h4>
    </div>
    <div class="card-body">
        <h6 class="font-weight-bold">
            <a href="{{ route('companies.show', $job->company) }}">{{ $job->company->name }}</a>
        </h6>
        {!! $job->company->description !!}
    </div>
</div>
