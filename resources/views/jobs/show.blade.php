@extends('layouts.app')
@section('title')
    {{ $job->title }}
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>{{ $job->title }}</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('jobs.index') }}"
                   class="btn btn-primary form-btn float-right">Back</a>
            </div>
        </div>
        @include('stisla-templates::common.errors')
        <div class="section-body">
            <div class="row mt-sm-4">
                <div class="col-12 col-md-12 col-lg-7">
                    @include('jobs.show_fields')
                </div>
                <div class="col-12 col-md-12 col-lg-5">
                    @include('jobs.apply_form')
                </div>
            </div>
        </div>
    </section>
@endsection
