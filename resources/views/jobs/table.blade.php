<div class="search-results-container">
    <div class="card">
        <div class="card-header">
            <h4>@yield('results-title')</h4>
        </div>
        <div class="card-body">
            <ul class="list-unstyled list-unstyled-border">
            @foreach($jobs as $job)
                <li class="media results-item">
                    <div class="result-item-logo mr-3 mb-2">
                        <a href="{{ route('companies.show', $job->company) }}">
                            <img class="w-100" src="{{ asset($job->company->logo ?? 'assets/images/missing-logo.svg') }}" alt="logo" width="50">
                        </a>
                    </div>
                    <div class="media-body">
                        <div class="float-right text-primary">
                            <ul class="results-item-details">
                                <li>
                                    <i class="fa fa-disk"></i><span>{{ __ucf('salary') }}</span>{{ money($job->salary_min) }} to {{ money($job->salary_max) }} per {{ $job->salary_type }}
                                    @if ($job->negotiable)
                                        <br><span class="text-small text-info">{{ __ucf('Negotiable') }}</span>
                                    @endif
                                </li>
                                <li>
                                    @if(auth_admin() && $job->status !== 'published')
                                        <a href="{{ route('jobs.approve', $job) }}" class="btn btn-warning">Approve</a>
                                    @endif
                                </li>
                            </ul>
                        </div>
                        <div class="media-title">
                            <a href="{{ route('jobs.show', $job) }}">{{ $job->title }}</a>
                        </div>
                        <ul class="text-small text-muted ul-inline">
                            <li><i class="fa fa-disk"></i><span>{{ __ucf('post type') }}: </span>{{ $job->post_type }}</li>
                            <li><i class="fa fa-disk"></i><span>{{ __ucf('start date') }}: </span>{{ human_date($job->start_date) }}</li>
                        </ul>
                        <div class="results-item-description">
                            <p>{!! $job->excerpt !!}</p>
                        </div>
                        <div class="results-item-footer">
                            <ul class="results-item-meta text-small text-muted ul-inline">
                                <li><i class="fa fa-briefcase"></i><span>{{ __ucf('category') }}: </span>{{ $job->category->name }}</li>
                                <li><i class="fa fa-map-pin"></i><span>{{ __ucf('location') }}: </span>{{ $job->city }}, {{ \App\Models\Country::$list[$job->country] }}</li>
                                <li><i class="fa fa-user"></i><span>{{ __ucf('contact') }} </span>{{ $job->contact_name }}</li>
                            </ul>
                            <ul class="results-item-tags ul-inline">
                                @foreach($job->tags as $tag)
                                    <li><i class="fa fa-tag"></i><span>{{ __ucf($tag->name) }}</span></li>
                                @endforeach
                            </ul>
                            @if(auth()->user()?->is_admin())
                                <div class="results-item-actions text-center">
                                    {!! Form::open(['route' => ['jobs.destroy', $job->id], 'method' => 'delete']) !!}
                                    <div class='btn-group'>
                                        <a href="{!! route('jobs.show', [$job->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                                        <a href="{!! route('jobs.edit', [$job->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("Are you sure want to delete this record ?")']) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            @endif
                        </div>
                    </div>
                </li>
            @endforeach
            </ul>
            <div class="text-center pt-1 pb-1">
                <a href="{{ route('jobs.index') }}" class="btn btn-primary btn-lg btn-round">
                    View All
                </a>
            </div>
        </div>
    </div>
</div>
