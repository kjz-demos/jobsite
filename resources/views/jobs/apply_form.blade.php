    <div class="card profile-widget">
        <form method="post" class="needs-validation" novalidate="">
            <div class="card-header">
                <h4>Apply</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-12">
                        <label>First Name</label>
                        <input type="text" class="form-control" value="" required="">
                        <div class="invalid-feedback">
                            Please fill in the first name
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <label>Last Name</label>
                        <input type="text" class="form-control" value="" required="">
                        <div class="invalid-feedback">
                            Please fill in the last name
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-12">
                        <label>Email</label>
                        <input type="email" class="form-control" value="" required="">
                        <div class="invalid-feedback">
                            Please fill in the email
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <label>Phone</label>
                        <input type="tel" class="form-control" value="">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-12">
                        <label>Message</label>
                        <textarea class="form-control summernote-simple"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-12">
                        <label>Upload CV</label>
                        <input type="file" class="form-control" accept="application/pdf" required="">
                        <div class="invalid-feedback">
                            Please fill in the email
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button class="btn btn-primary">Send application</button>
            </div>
        </form>
    </div>
