@push('page_css')
<link rel="stylesheet" href="{{ asset('assets/modules/summernote/summernote-bs4.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">
@endpush

<div class="form-group row mb-4">
    <div class="col-sm-12 col-md-7 offset-md-5">
        <p class="text-bold">About your company</p>
    </div>
</div>

<!-- Company Id Field -->
<div class="form-group row mb-4">
    {!! Form::label('name', 'Company name:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => true]) !!}
    </div>
</div>

<!-- Company email -->
<div class="form-group row mb-4">
    {!! Form::label('email', 'Company email:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::email('email', null, ['class' => 'form-control', 'required' => true]) !!}
    </div>
</div>

<!-- Company phone -->
<div class="form-group row mb-4">
    {!! Form::label('phone', 'Company phone:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::text('phone', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Contact Name Field -->
<div class="form-group row mb-4">
    {!! Form::label('contact_name', 'Contact Name:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::text('contact_name', null, ['class' => 'form-control', 'required' => true]) !!}
    </div>
</div>

<!-- Company logo -->
<div class="form-group row mb-4">
    {!! Form::label('logo', 'Company logo:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        <div id="logo-preview" class="image-preview">
            <label id="logo-label">Choose File</label>
            {!! Form::file('logo', null, ['class' => 'form-control', 'accept' => 'image/x-png,image/gif,image/jpeg']) !!}
        </div>
    </div>
</div>

<!-- Company description -->
<div class="form-group row mb-4">
    {!! Form::label('description', 'Company description:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::textarea('description', null, ['class' => 'form-control summernote', 'required' => true]) !!}
    </div>
</div>

<div class="form-group row mb-4">
    <div class="col-sm-12 col-md-7 offset-md-5">
        <p class="text-bold">About the job</p>
    </div>
</div>

<!-- Title Field -->
<div class="form-group row mb-4">
    {!! Form::label('title', 'Title:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::text('title', null, ['class' => 'form-control', 'maxlength' => 128, 'required' => true]) !!}
    </div>
</div>

<!-- Slug Field -->
<div class="form-group row mb-4 d-none">
    {!! Form::label('slug', 'Slug:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::text('slug', null, ['class' => 'form-control', 'maxlength' => 128]) !!}
    </div>
</div>

<!-- Category Field -->
<div class="form-group row mb-4">
    {!! Form::label('category_id', 'Category:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::select('category_id', ['' => 'Select category'] + \App\Models\Category::pluck('name', 'id')->toArray(), null, ['class' => 'form-control', 'required' => true]) !!}
    </div>
</div>

<!-- Post Type Field -->
<div class="form-group row mb-4">
    {!! Form::label('post_type', 'Job Type:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::select('post_type', \App\Models\Job::$types, null, ['class' => 'form-control', 'required' => true]) !!}
    </div>
</div>

<!-- Commute Type Field -->
<div class="form-group row mb-4">
    {!! Form::label('post_type', 'Job Type:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::select('post_type', ['' => 'Select commute type'] + \App\Models\Job::$commute, null, ['class' => 'form-control', 'required' => true]) !!}
    </div>
</div>

<!-- Salary Min Field -->
<div class="form-group row mb-4">
    {!! Form::label('salary_min', 'Salary Min:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::number('salary_min', null, ['class' => 'form-control', 'required' => true]) !!}
    </div>
</div>

<!-- Salary Max Field -->
<div class="form-group row mb-4">
    {!! Form::label('salary_max', 'Salary Max:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::text('salary_max', null, ['class' => 'form-control', 'required' => true]) !!}
    </div>
</div>

<!-- Salary Type Field -->
<div class="form-group row mb-4">
    {!! Form::label('salary_type', 'Salary Type:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::select('salary_type', ['hour' => 'Per hour', 'day' => 'Per day', 'month' => 'Per month', 'year' => 'Per year'], null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Negotiable Field -->
<div class="form-group row mb-4">
    {!! Form::label('negotiable', 'Salary negotiable:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::checkbox('negotiable', '1', null, ['id' => 'negotiable', 'class' => '']) !!}
    </div>
</div>

<!-- Start Date Field -->
<div class="form-group row mb-4">
    {!! Form::label('start_date', 'Start Date:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::date('start_date', null, ['class' => 'form-control', 'id' => 'start_date']) !!}
    </div>
</div>

<!-- Excerpt Field -->
<div class="form-group row mb-4">
    {!! Form::label('excerpt', 'Excerpt:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::textarea('excerpt', null, ['class' => 'form-control', 'rows' => 3]) !!}
    </div>
</div>

<!-- Description Field -->
<div class="form-group row mb-4">
    {!! Form::label('description', 'Description:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::textarea('description', null, ['class' => 'form-control summernote']) !!}
    </div>
</div>

<!-- Country Field -->
<div class="form-group row mb-4">
    {!! Form::label('country', 'Country:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::select('country', ['' => 'Select country'] + \App\Models\Country::$list, null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- City Field -->
<div class="form-group row mb-4">
    {!! Form::label('city', 'City:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::text('city', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Tags[] Field -->
<div class="form-group row mb-4">
    {!! Form::label('tags', 'Tags:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::text('tags', null, ['class' => 'form-control inputtags']) !!}
    </div>
</div>

<!-- Captcha Field -->
<div class="form-group row mb-4">
    {!! Form::label('captcha', 'Captcha:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        <p class="text-small">What is {{ rand(1,5) + rand(1,10) }}</p>
        {!! Form::text('captcha', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Accept Terms Field -->
<div class="form-group row mb-4">
    {!! Form::label('accept_terms', 'Terms and condition:', ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']) !!}
    <div class="col-sm-12 col-md-7">
        <p class="text-small">I've read the and I agree with the terms and conditions of the site</p>
        {!! Form::checkbox('accept_terms', '1', null, ['id' => 'accept_terms', 'required' => true]) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="form-group row mb-4">
    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('jobs.index') }}" class="btn btn-light">Cancel</a>
</div>

@push('page_js')
<script src="{{ asset('assets/modules/summernote/summernote-bs4.js') }}"></script>
<script src="{{ asset('assets/modules/moment.min.js') }}"></script>
<script src="{{ asset('assets/modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/modules/upload-preview/assets/js/jquery.uploadPreview.min.js') }}"></script>
<script src="{{ asset('assets/modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
<script>
    "use strict";
    $.uploadPreview({
        input_field: "#logo", 
        preview_box: "#logo-preview",
        label_field: "#logo-label",  
        label_default: "Choose File", 
        label_selected: "Change File",
        no_label: false,              
        success_callback: null        
    });
    $(".inputtags").tagsinput('items');
    $('.bootstrap-tagsinput').addClass('w-100')
</script>
@endpush