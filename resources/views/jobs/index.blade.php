@extends('layouts.app')
@section('title')
    Jobs
@endsection
@section('results-title')
    {{ __ucf('all jobs') }}
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Jobs</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('jobs.create')}}" class="btn btn-primary form-btn">Job <i class="fas fa-plus"></i></a>
            </div>
        </div>
    <div class="section-body">
       <div class="card">
            <div class="card-body">
                @include('jobs.table')
            </div>
       </div>
   </div>

    </section>
@endsection

