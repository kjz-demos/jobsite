<div class="table-responsive">
    <table class="table" id="pages-table">
        <thead>
            <tr>
                <th>Title</th>
        <th>Content</th>
        <th>Author Id</th>
        <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($pages as $page)
            <tr>
                       <td>{{ $page->title }}</td>
            <td>{{ $page->content }}</td>
            <td>{{ $page->author_id }}</td>
            <td>{{ $page->status }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['pages.destroy', $page->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('pages.show', [$page->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('pages.edit', [$page->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("Are you sure want to delete this record ?")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
        @endforeach
        </tbody>
    </table>
</div>
