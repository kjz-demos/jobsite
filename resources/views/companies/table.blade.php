<div class="table-responsive">
    <table class="table" id="companies-table">
        <thead>
            <tr>
                <th>Logo</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                @if (auth_admin())
                <th colspan="3">Action</th>
                @endif
            </tr>
        </thead>
        <tbody>
        @foreach($companies as $company)
            <tr>
                <td><img src="{{ asset($company->logo ?? 'assets/images/missing-logo.svg') }}" alt="N/A" width="60"></td>
                <td><a href="{{route('companies.show', $company)}}">{{ $company->name }}</a></td>
                <td>{{ $company->email }}</td>
                <td>{{ $company->phone }}</td>
                @if (auth_admin())
                   <td class=" text-center">
                       {!! Form::open(['route' => ['companies.destroy', $company->id], 'method' => 'delete']) !!}
                       <div class='btn-group'>
                           <a href="{!! route('companies.show', [$company->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                           <a href="{!! route('companies.edit', [$company->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                           {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("Are you sure want to delete this record ?")']) !!}
                       </div>
                       {!! Form::close() !!}
                   </td>
                @endif
               </tr>
        @endforeach
        </tbody>
    </table>
</div>
