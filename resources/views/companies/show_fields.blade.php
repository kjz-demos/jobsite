<div class="row">
    <div class="col-md-4 mb-4">
        <img src="{{ asset($company->logo ?? 'assets/images/missing-logo.svg') }}" class="w-100" alt="">
    </div>
    <div class="col-md-8 mb-4">
        <h4>{{ $company->name }}</h4>
        <!-- Email Field -->
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            <p>{{ $company->email }}</p>
        </div>
        <!-- Phone Field -->
        <div class="form-group">
            {!! Form::label('phone', 'Phone:') !!}
            <p>{{ $company->phone }}</p>
        </div>
    </div>
    <div class="col-12">
        <div class="mb-4">
            {!! $company->description !!}            
        </div>
        @if ( auth_admin() )
            <div class="form-group">
                <a href="{{ route('companies.edit', $company) }}" class="btn btn-warning">Edit</a>
            </div>
        @endif
    </div>
</div>


