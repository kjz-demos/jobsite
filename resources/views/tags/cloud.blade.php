@foreach($tags as $tag)
    <a href="{{ route('tags.show', $tag) }}" class="btn-sm btn-info btn-icon icon-left ml-2 mb-2 d-inline-block">
        <i class="fa fa-tag"></i>
        {{ $tag->name }} ({{ $tag->jobs()->count() }})
    </a>
@endforeach
