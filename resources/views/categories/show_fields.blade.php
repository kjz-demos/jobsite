<table class="table table-sm">
    <tr><td>Icon:</td><td><i class="{{ $category->icon }}"></i>{{ $category->icon }}</td></tr>
    <tr><td>Image:</td><td><img src="{{ asset($category->image ?? 'assets/images/missing-logo.svg') }}" alt="N/A" width="80"></td></tr>
    <tr><td>Banner:</td><td><img src="{{ asset($category->banner ?? 'assets/images/missing-logo.svg') }}" alt="N/A" width="300"></td></tr>
    <tr><td>Created At:</td><td>{{ $category->created_at }}</td></tr>
    <tr><td>Updated At:</td><td>{{ $category->updated_at }}</td></tr>
</table>
