@extends('layouts.app')
@section('title')
    {{ $category->name }}
@endsection
@section('results-title')
    Jobs in {{ $category->name }}
@endsection
@section('content')
    <section class="section">
        <div class="jumbotron">
            <h1>{{ __ucf('category') }}: {{ $category->name }}</h1>
            <div class="jumbotron-body">
                {!! $category->description !!}
            </div>
        </div>
        @include('stisla-templates::common.errors')
        <div class="section-body">
            @if(auth_admin())
            <div class="card">
                <div class="card-body">
                    @include('categories.show_fields')
                </div>
            </div>
            @endif
            <div class="card">
                <div class="card-body">
                    @include('jobs.table', ['jobs' => $category->jobs])
                </div>
            </div>
        </div>
    </section>
@endsection
