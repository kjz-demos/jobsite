
<div class="table-responsive">
    <table class="table table-striped" id="categories-table">
        <thead>
            <tr>
                <th>Name</th>
                <th class="text-right">Count</th>
                @if(auth_admin())
                <th colspan="3">Action</th>
                @endif
            </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
            <tr>
                <td>
                    <a href="{{ route('categories.show', $category) }}">
                        <i class="{{ $category->icon }}"></i>
                        {{ $category->name }}
                    </a>
                </td>
                <td class="text-right">{{ $category->jobs->count() }}</td>
                @if(auth_admin())
                <td class=" text-center">
                   {!! Form::open(['route' => ['categories.destroy', $category->id], 'method' => 'delete']) !!}
                   <div class='btn-group'>
                       <a href="{!! route('categories.show', [$category->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                       <a href="{!! route('categories.edit', [$category->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                       {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("Are you sure want to delete this record ?")']) !!}
                   </div>
                   {!! Form::close() !!}
                </td>
                @endif
           </tr>
        @endforeach
        </tbody>
    </table>
</div>
