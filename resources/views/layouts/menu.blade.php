<li class="side-menus {{ Request::is('dashboard') ? 'active' : '' }}">
    <a class="nav-link" href="/">
        <span>Home</span>
    </a>
</li>
<li class="nav-item {{ Request::is('categories*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('categories.index') }}">
        <span>Categories</span>
    </a>
</li>
<li class="nav-item {{ Request::is('companies*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('companies.index') }}">
        <span>Companies</span>
    </a>
</li>
<li class="nav-item {{ Request::is('jobs*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('jobs.index') }}">
        <span>Jobs</span>
    </a>
</li>
<li class="nav-item {{ Request::is('tags*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('tags.index') }}">
        <span>Tags</span>
    </a>
</li>
