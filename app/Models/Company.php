<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;

/**
 * Class Company
 * @package App\Models
 * @version June 15, 2022, 6:22 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $jobs
 * @property string $name
 * @property string $email
 * @property string $logo
 * @property string $banner
 * @property string $description
 * @property string $phone
 * @property boolean $accept_marketing_offers
 */
class Company extends Model
{
    use SoftDeletes, HasFactory, Notifiable;

    public $table = 'companies';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'email',
        'logo',
        'banner',
        'description',
        'phone',
        'accept_marketing_offers'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'logo' => 'string',
        'banner' => 'string',
        'phone' => 'string',
        'accept_marketing_offers' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|max:128',
        'email' => 'required|email|max:64',
        'logo' => 'mimes:png,jpg,jpeg,gif|max:256',
        'banner' => 'mimes:png,jpg,jpeg,gif|max:512',
        'phone' => 'string|max:32'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function jobs()
    {
        return $this->hasMany(\App\Models\Job::class);
    }
}
