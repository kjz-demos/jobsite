<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Category
 * @package App\Models
 * @version June 15, 2022, 6:22 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $jobs
 * @property string $name
 * @property string $description
 * @property string $icon
 * @property string $image
 * @property string $banner
 */
class Category extends Model
{
    use HasFactory;

    public $table = 'categories';
    
    public $fillable = [
        'name',
        'description',
        'icon',
        'image',
        'banner'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'icon' => 'string',
        'image' => 'string',
        'banner' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|max:64',
        'description' => 'max:256',
        'icon' => 'max:32',
        'image' => 'max:512|mimes:png,jpg,jpeg,gif',
        'banner' => 'max:1024|mimes:png,jpg,jpeg'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function jobs()
    {
        return $this->hasMany(\App\Models\Job::class);
    }
}
