<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Page
 * @package App\Models
 * @version June 15, 2022, 6:21 pm UTC
 *
 * @property \App\Models\User $author
 * @property string $title
 * @property string $slug
 * @property string $image
 * @property string $content
 * @property integer $author_id
 * @property string $status
 */
class Page extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'pages';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'slug',
        'image',
        'content',
        'author_id',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'image' => 'string',
        'author_id' => 'integer',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|max:128',
        'slug' => 'max:128',
        'image' => 'max:512|mimes:png,jpeg,jpg',
        'content' => 'required',
        'status' => 'in:published,pending,archived'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function author()
    {
        return $this->belongsTo(\App\Models\User::class, 'author_id', 'id');
    }
}
