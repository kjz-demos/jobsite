<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Job
 * @package App\Models
 * @version June 15, 2022, 6:22 pm UTC
 *
 * @property \App\Models\Category $category
 * @property \App\Models\Company $company
 * @property \Illuminate\Database\Eloquent\Collection $tags
 * @property string $title
 * @property string $slug
 * @property integer $category_id
 * @property string $post_type
 * @property integer $salary_min
 * @property integer $salary_max
 * @property string $salary_type
 * @property boolean $negotiable
 * @property string $start_date
 * @property string $country
 * @property string $contact_name
 * @property string $city
 * @property boolean $accept_terms
 * @property integer $company_id
 */
class Job extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'jobs';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'slug',
        'category_id',
        'post_type',
        'salary_min',
        'salary_max',
        'salary_type',
        'negotiable',
        'start_date',
        'status',
        'excerpt',
        'description',
        'country',
        'contact_name',
        'city',
        'accept_terms',
        'company_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'category_id' => 'integer',
        'post_type' => 'string',
        'salary_min' => 'integer',
        'salary_max' => 'integer',
        'salary_type' => 'string',
        'negotiable' => 'boolean',
        'country' => 'string',
        'contact_name' => 'string',
        'city' => 'string',
        'captcha' => 'string',
        'accept_terms' => 'boolean',
        'company_id' => 'integer'
    ];

    public static $types = [
        'full-time' => 'Full time',
        'part-time' => 'Part time',
        'temporary' => 'Temporary',
        'contract' => 'Contract',
        'internship' => 'Internship',
        'permanent' => 'Permanent',
        'optional' => 'Optional'
    ];

    public static $commute = [
        'remote' => 'Remote',
        'on-site' => 'On site',
        'optional' => 'Optional'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|max:128',
        'slug' => 'max:128',
        'category_id' => 'required',
        'start_date' => 'required|date',
        'description' => 'string|required',
        'excerpt' => 'string|required|max:256',
        'country' => 'required|max:2|min:2',
        'city' => 'required',
        'captcha' => 'required',
        'accept_terms' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function tags()
    {
        return $this->belongsToMany(\App\Models\Tag::class);
    }
}
