<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Job;
use App\Models\Tag;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [
            'categories' => Category::all(),
            'jobs' => Job::where('status', 'published')->orderBy('id', 'desc')->take(25)->get(),
            'tags' => Tag::all() // TODO: Only list those with something in them
        ];
        return view('home', $data);
    }
}
