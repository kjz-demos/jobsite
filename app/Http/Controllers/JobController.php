<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateJobRequest;
use App\Http\Requests\UpdateJobRequest;
use App\Models\Company;
use App\Models\Job;
use App\Models\Tag;
use App\Notifications\JobPublished;
use App\Notifications\NewJob;
use App\Repositories\JobRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Laracasts\Flash\Flash;
use Response;

class JobController extends AppBaseController
{
    /** @var JobRepository $jobRepository*/
    private $jobRepository;

    public function __construct(JobRepository $jobRepo)
    {
        $this->jobRepository = $jobRepo;
    }

    /**
     * Display a listing of the Job.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $jobs = auth_admin() ? $this->jobRepository->all() : Job::where('status', 'published')->get();

        return view('jobs.index')
            ->with('jobs', $jobs);
    }

    /**
     * Show the form for creating a new Job.
     *
     * @return Response
     */
    public function create()
    {
        return view('jobs.create');
    }

    /**
     * Store a newly created Job in storage.
     *
     * @param CreateJobRequest $request
     *
     * @return Response
     */
    public function store(CreateJobRequest $request)
    {
        $company = Company::firstOrCreate(
            [ 'email' => $request->email ],
            $request->only(['name', 'email', 'logo', 'banner', 'description', 'phone'])
        );
        $computed = [
            'slug' => slug($request->title) . '_' . strtolower(\Str::random(3)),
            'company_id' => $company->id,
            'excerpt' => nl2br($request->excerpt)
        ];
        $input = array_merge($request->all(), $computed);

        $job = $this->jobRepository->create($input);

        $tags = explode(',', $request->tags);
        foreach ($tags as $tag) {
            $new_tag = Tag::firstOrCreate(['name' => $tag]);
            $job->tags()->attach($new_tag->id);
        }

        if($request->logo) {
            $logo = $request->file('logo');
            $filename = $company->id . '_' . slug($company->name) . '.' . $logo->extension();
            $logo->move('storage/logos/', $filename);
            $company->logo = "storage/logos/$filename";
            $company->save();
        }

        foreach (User::where('role', 'admin')->get() as $admin) {
            Notification::send($admin, new NewJob($job));
        }

        Flash::success('Job saved successfully.');

        return redirect(route('jobs.index'));
    }

    /**
     * Display the specified Job.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $job = $this->jobRepository->find($id);

        if (empty($job)) {
            Flash::error('Job not found');

            return redirect(route('jobs.index'));
        }

        return view('jobs.show')->with('job', $job);
    }

    /**
     * Show the form for editing the specified Job.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $job = $this->jobRepository->find($id);

        if (empty($job)) {
            Flash::error('Job not found');

            return redirect(route('jobs.index'));
        }

        return view('jobs.edit')->with('job', $job);
    }

    /**
     * Update the specified Job in storage.
     *
     * @param int $id
     * @param UpdateJobRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJobRequest $request)
    {
        $job = $this->jobRepository->find($id);

        if (empty($job)) {
            Flash::error('Job not found');

            return redirect(route('jobs.index'));
        }

        $job = $this->jobRepository->update($request->all(), $id);

        Flash::success('Job updated successfully.');

        return redirect(route('jobs.index'));
    }

    /**
     * Remove the specified Job from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $job = $this->jobRepository->find($id);

        if (empty($job)) {
            Flash::error('Job not found');

            return redirect(route('jobs.index'));
        }

        $this->jobRepository->delete($id);

        Flash::success('Job deleted successfully.');

        return redirect(route('jobs.index'));
    }

    public function approve($id)
    {
        if (!auth_admin())
            abort(403, 'You\'re not allowed to do that!');

        $job = $this->jobRepository->find($id);

        $job->status = 'published';
        $job->save();

        Notification::send($job->company, new \App\Notifications\JobPublished($job));

        Flash::success('Job approved successfully.');

        return redirect()->back();
    }
}
