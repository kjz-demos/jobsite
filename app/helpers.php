<?php

/**
 * Return full page name
 * @return string
 */
function page_name($title = null)
{
    if ( ! $title ) {
        $title = str_replace( '.', ' ', \Request::route()->getName());
        $title = $title ?: str_replace('\\', ' ', \Request::path());
    }
    return ucfirst(str_replace(['-', '_'], ' ', $title));
}

/**
 * Return full page title
 * @return string
 */
function page_title($title = null)
{
    return page_name($title) . ' | ' . config('app.name') . ' - Anonymous file sharing';
}

/**
 * Translate and capitalise
 * @param $string
 * @return string
 */
function __ucf($string = '')
{
    return ucfirst(__($string));
}


/**
 * Generate associative array from database results
 *
 * @param Illuminate\Database\Eloquent\Collection $data
 * @return array
 */
function id_assoc( $data, $value_column )
{
    $array = [];
    foreach ( $data as $row ) {
        $array[$row->id] = $row->{$value_column};
    }
    return $array;
}

/**
 * Turn a given string into a slug
 *
 * @param string $string
 * @param string $glue
 * @return string
 */
function slug( $string, $glue = '_' )
{
    $string = strtolower( str_replace( ' ', $glue, $string ) );
    $string = str_replace( ['"', '\''], '', $string );
    return $string;
}

/**
 * Turn a slug into human friendly string
 *
 * @param string $string
 * @param string $glue
 * @return string
 */
function unslug( $string )
{
    $string = str_replace( ['-', '_'], ' ', $string );
    return $string;
}

/**
 * Format an amount of money
 * @param float $amount
 * @return string
 */
function money( float $amount )
{
    return '€' . number_format( $amount, 2, '.', ',' );
}

/**
 * Format number
 * @param float|int $number
 * @return string
 */
function human_int( $number )
{
    return number_format( $number, 0, '.', ',' );
}

/**
 * Format a date
 * @param string $string
 * @return string
 */
function human_date( $string )
{
    if ($string) {
        if ($string instanceof \Carbon\Carbon) {
            $time = $string->timestamp;
        } else {
            $time = \strtotime( $string );
        }
       return date( 'd M Y', $time );
    }

    return 'N/A';
}

/**
 * Format a date
 * @param string $string
 * @return string
 */
function human_datetime( $string )
{
    if ($string) {
        if ($string instanceof \Carbon\Carbon){
          $time = $string;
        }
        else {
            $time = \Carbon\Carbon::create($string);
        }
        $format = $time->isBefore(\Carbon\Carbon::create('today')) || $time->isAfter(\Carbon\Carbon::create('tomorrow')) ? 'd M Y H:i' : 'H:i';
        return $time->format($format);
    }

    return  'N/A';
}

/**
 * Create a carbon instance from a time string
 * @param string $string
 * @return \Carbon\Carbon
 */
function carbon( $string = null )
{
    if ($string instanceof \Carbon\Carbon)
    {
        return $string;
    }

    return $string ? \Carbon\Carbon::create( $string ) : \Carbon\Carbon::now();
}

/**
 * Return Carbon instance for current time
 * @return \Carbon\Carbon
 */
function carbon_now()
{
    return \Carbon\Carbon::now();
}

/**
 * Replace <br> with a new line in a string
 * @return string
 */
function br2nl( string $string )
{
    $string = str_replace( ['<br>', '<br/>', '<br />', '<BR>', '<BR/>', '<BR />'], "\n", $string );
    return $string;
}

/**
 * Return full name of logged in user
 * @return string
 */
function full_name()
{
    return auth()->check() ? auth()->user()->firstname . ' ' . auth()->user()->lastname : '';
}

/**
 * Is user logged in and an admin
 * @return string
 */
function auth_admin()
{
    return auth()->user()?->role === 'admin';
}

/**
 * Explode with multiple delimiters
 * @param string $string
 * @return string
 */
function multiexplode( array $delimiters, string $string )
{
    $string = str_replace( $delimiters, $delimiters[0], $string );
    return explode( $delimiters[0], $string );
}

/**
 * Settings Helper
 * @param string $key
 * @return string
 */
function settings(string $key){
    if ($setting = \App\Models\Setting::where('name',$key)->first()){
        return $setting->value;
    }

    return null;
}

if (! function_exists('upload_image') ) {
    /**
     * Save uploaded image with a specified path and quality
     * @param Illuminate\Http\UploadedFile|string $file
     * @param string $path
     * @param int $quality
     * @return string
     */
    function upload_image($file, string $path = 'storage/images', int $quality = 80 )
    {
        $img = \Image::make($file instanceof \Illuminate\Http\UploadedFile ? $file->getRealPath() : $file);
        $img->resize(1200, 1200, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $path = rtrim( $path, '/') . '/' . time() . '_' . Str::random(4) . '.jpg';

        $img->save( $path, $quality );

        return $path;
    }
}


/**
 * Convert total minutes to hour format
 *  110 -> 01:50
 */
function MinutesToHoursMins($time, $format = '%02d:%02d')
{
    if (!is_numeric($time))
    {
        return sprintf($format, '00', '00');
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}
/**
 * Convert hour format to total minutes
 *  1:50 -> 110
 */
function HoursMinsToMinutes($time)
{
    $time = explode(':', $time);
    return ((is_numeric($time[0]) ? $time[0] : 0 ) * 60) + (is_numeric(($time[1] ?? 0)) ? ($time[1] ?? 0) : 0 );
}

/**
 * Multi-byte version of substr_replace() in PHP 7+
 */
if (function_exists('mb_substr_replace') === false)
{
    function mb_substr_replace($string, $replacement, $start, $length = null, $encoding = null)
    {
        if (extension_loaded('mbstring') === true)
        {
            $string_length = (is_null($encoding) === true) ? mb_strlen($string) : mb_strlen($string, $encoding);

            if ($start < 0)
            {
                $start = max(0, $string_length + $start);
            }
            else
            if ($start > $string_length)
            {
                $start = $string_length;
            }

            if ($length < 0)
            {
                $length = max(0, $string_length - $start + $length);
            }
            else
            if ((is_null($length) === true) || ($length > $string_length))
            {
                $length = $string_length;
            }

            if (($start + $length) > $string_length)
            {
                $length = $string_length - $start;
            }

            if (is_null($encoding) === true)
            {
                return mb_substr($string, 0, $start) . $replacement . mb_substr($string, $start + $length, $string_length - $start - $length);
            }

            return mb_substr($string, 0, $start, $encoding) . $replacement . mb_substr($string, $start + $length, $string_length - $start - $length, $encoding);
        }

        return (is_null($length) === true) ? substr_replace($string, $replacement, $start) : substr_replace($string, $replacement, $start, $length);
    }
}
