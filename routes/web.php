<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('pages', App\Http\Controllers\PageController::class);

Route::resource('categories', App\Http\Controllers\CategoryController::class);

Route::resource('companies', App\Http\Controllers\CompanyController::class);

Route::group(['middleware' => 'auth'], function(){
    Route::get('jobs/{job}/approve', [App\Http\Controllers\JobController::class, 'approve'])->name('jobs.approve');
});

Route::resource('jobs', App\Http\Controllers\JobController::class);

Route::resource('tags', App\Http\Controllers\TagController::class);

Route::resource('users', App\Http\Controllers\UserController::class);
