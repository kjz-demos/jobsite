<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         User::create([
             'firstname' => 'Brian',
             'lastname' => 'K',
             'email' => 'brian@kajuzi.com',
             'role' => 'admin',
             'password' => bcrypt('password'),
             'email_verified_at' => now(),
         ]);
         User::create([
             'firstname' => 'Thomas',
             'lastname' => ' ',
             'email' => 'thomas@example.com',
             'role' => 'admin',
             'password' => bcrypt('password'),
             'email_verified_at' => now(),
         ]);

         $categories = [
             "Engineering", "Financial Services", "Banking", "Security & Safety", "Training",
             "Public Service", "Real Estate", "Independent & Freelance", "IT & Telecoms", "Marketing & Communication",
             "Babysitting & Nanny Work", "Human Resources", "Medical & Healthcare", "Tourism & Restaurants", "Transportation & Logistics",
         ];

         foreach ($categories as $category) {
            Category::create(['name' => $category]);
         }

        \App\Models\Tag::factory(10)->create();
        \App\Models\Job::factory(25)->create();
    }
}
