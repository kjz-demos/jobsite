<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('slug', 128)->unique()->nullable();
            $table->foreignId('category_id')->constrained();
            $table->foreignId('company_id')->constrained();
            $table->string('post_type', 32)->nullable();
            $table->string('commute', 32)->nullable();
            $table->integer('salary_min');
            $table->integer('salary_max');
            $table->enum('salary_type', ['hour','day','month','year']);
            $table->boolean('negotiable')->nullable();
            $table->date('start_date');
            $table->string('excerpt', 256);
            $table->text('description');
            $table->string('country', 2);
            $table->enum('status', ['pending', 'published'])->default('pending');
            $table->string('contact_name')->nullable();
            $table->string('city', 64);
            $table->boolean('accept_terms');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign('jobs_category_id_foreign');
        $table->dropForeign('jobs_company_id_foreign');
        $table->dropIndex('jobs_category_id_index');
        $table->dropIndex('jobs_company_id_index');
        Schema::drop('jobs');
    }
}
