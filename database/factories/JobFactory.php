<?php

namespace Database\Factories;

use App\Models\Job;
use Illuminate\Database\Eloquent\Factories\Factory;

class JobFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Job::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->words(3, true);
        return [
            'title' => ucfirst($title),
            'slug' => slug($title),
            'category_id' => $this->faker->randomDigitNotNull,
            'post_type' => $this->faker->randomElement(\App\Models\Job::$types),
            'salary_min' => $this->faker->randomElement([2000,2500,3000,3500]),
            'salary_max' => $this->faker->randomElement([4000,4500,5000,5500]),
            'salary_type' => $this->faker->randomElement(['day', 'month']),
            'negotiable' => $this->faker->randomElement([0,1]),
            'start_date' => carbon()->endOfMonth(),
            'description' => $this->faker->paragraphs(rand(2,5), true),
            'excerpt' => $this->faker->paragraph(1),
            'status' => $this->faker->randomElement(['pending', 'published']),
            'country' => $this->faker->randomElement(array_keys(\App\Models\Country::$list)),
            'contact_name' => $this->faker->firstName,
            'city' => ucfirst($this->faker->word),
            'accept_terms' => 1,
            'company_id' => \App\Models\Company::factory(),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];
    }
}
